/* При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться 
единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с 
помощью Javascript.
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку 
"Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг 
должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево. */

window.onload = () => {
    const butCircle = document.getElementById("circle");

    // создание кнопки "Нарисовать"
    const createCircle = document.createElement("input");
    createCircle.setAttribute("type", "button");
    createCircle.setAttribute("value", "Нарисовать");
    createCircle.setAttribute("id", "createCircle");

    // поле для ввода диаметра круга
    const diam = document.createElement("input");
    diam.setAttribute("type", "text");
    diam.setAttribute("placeholder", "Введите диаметр круга");
    diam.setAttribute("value", "");

    // создаем событие по клику на кнопку "Нарисовать круг"
    butCircle.onclick = () => {
        butCircle.remove();
        document.getElementById("main");
        main.append(createCircle);
        main.append(diam)
    }

    // создаем событие по клику на кнопку "Нарисовать"
    createCircle.onclick = () => {
        // рисуем круги не 10*10, как в условии, а исходя из того, что ввел пользователь.
        // чтоб нарисовать 10*10, просто указать в circle.style.width и circle.style.height "10px"
        const diamCircle = diam.value + "px";
        createCircle.remove();
        diam.remove();

        // случайный цвет    
        const color = () => {
            let r = Math.floor(Math.random() * (256)),
                g = Math.floor(Math.random() * (256)),
                b = Math.floor(Math.random() * (256));
            return '#' + r.toString(16) + g.toString(16) + b.toString(16);
        }

        for (let i = 0; i < 100; i++) {
            const circle = document.createElement("div");
            circle.setAttribute("class", "circleStyle")
            circle.style.width = diamCircle;
            circle.style.height = diamCircle;
            circle.style.backgroundColor = color();
            main.append(circle);

            // создаем событие по клику на круг
            circle.onclick = () => {
                circle.remove();
            }
        };
    };
}

